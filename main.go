package main

import (
	"fmt"
	"os"
	"strconv"

	"external-sort/generator"
	"external-sort/sorter"
)

const USAGE string = `Usage: external-sort [command] [options]
Available commands:
generate [outfile] [n_lines] [line_length]
	Example:
	external-sort generate output.txt 1000 10 - this will generate file with 1000 random lines of length 10

sort [infile] [outfile] [chunk_size]
 	Example:
 	external-sort sort input.txt output.txt 1000 - this will sort lines from input.txt using temp files of 1000-lines lengths`

func main() {
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) < 4 {
		fmt.Println("Not enough arguments!")
		showUsage()
	}

	switch argsWithoutProg[0] {
	case "generate":
		outfile := argsWithoutProg[1]
		n_lines, err := strconv.ParseInt(argsWithoutProg[2], 10, 0)
		if err != nil {
			fmt.Println("Incorrect 'n_lines' argument")
			showUsage()
		}
		line_length, err := strconv.ParseInt(argsWithoutProg[3], 10, 0)
		if err != nil {
			fmt.Println("Incorrect 'line_length' argument")
			showUsage()
		}
		_, err = generator.Generate(outfile, int(n_lines), int(line_length))
		if err != nil {
			fmt.Printf("Generation failed: %s\n", err.Error())
			os.Exit(1)
		}
	case "sort":
		infile := argsWithoutProg[1]
		outfile := argsWithoutProg[2]
		chunk_size, err := strconv.ParseInt(argsWithoutProg[3], 10, 0)
		if err != nil {
			fmt.Println("Incorrect 'chunk_size' argument")
			showUsage()
		}
		sorter.Sort(infile, outfile, int(chunk_size))
	default:
		showUsage()
	}
}

func showUsage() {
	fmt.Println(USAGE)
	os.Exit(1)
}
