package generator

import (
	"log"
	"testing"
)

func TestGenerate(t *testing.T) {
	n, err := Generate(`../test1`, 100000000, 20)
	if err != nil {
		t.Error(err)
	}
	log.Printf("written bytes:%d\n", n)
}
