package generator

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

func Generate(filename string, n, length int) (bytes int, err error) {
	if n <= 0 {
		return 0, fmt.Errorf("Number of lines should be more than 0.")
	}
	if length <= 0 {
		return 0, fmt.Errorf("String length should be more than 0.")
	}
	var f *os.File
	f, err = os.Create(filename)
	defer f.Close()
	if err != nil {
		return
	}

	w := bufio.NewWriterSize(f, 8192) //defaultBufSize = 4096
	var written int
	tick := time.Tick(1 * time.Second)
	for i := 0; i < n; i++ {
		written, err = w.WriteString(RandString(length))
		bytes += written
		if err != nil {
			log.Fatalf("Error writing to file: %s", err.Error())
		}
		select {
		case <-tick:
			log.Printf(`%3.f%%, bytes written:%d`, float64(i)/float64(n)*100, bytes)
		default:
			continue
		}
	}

	w.Flush()
	log.Printf(`100%%, bytes written:%d`, bytes)
	return
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandString(n int) string {
	b := make([]byte, n+1)
	for i := 0; i < n; i++ {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	b[n] = '\n'
	return string(b)
}
