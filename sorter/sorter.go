package sorter

import (
	"bufio"
	"io"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
)

func Sort(infile, outfile string, chunkSize int) {

//	log.Println("Generating temp files.")
	tempFiles, err := generateTempFiles(infile, chunkSize)
	if err != nil {
		log.Fatalf("Error generating temp files: %s\n", err.Error())
	}
	defer deleteTempFiles(&tempFiles)

	nBufs := len(tempFiles)
	bufSize := chunkSize / nBufs
	mergeBufs := make([][]string, nBufs)
	mergeIdx := make([]int, nBufs)
	readers := make([]*bufio.Reader, nBufs)

	for i := range mergeBufs {
		mergeBufs[i] = make([]string, bufSize)
		mergeIdx[i] = bufSize //so bufs fill initially
		f, err := os.Open(tempFiles[i].Name())
		defer f.Close()
		if err != nil {
			log.Fatalf("Couldn't open temp files: %s\n", err.Error())
		}
		readers[i] = bufio.NewReader(f) //mb change buffer's size?
	}

	//out file
	out, err := os.Create(outfile)
	if err != nil {
		log.Fatalf("Error creating output file %s. Error:%s\n", outfile, err.Error())
	}
	defer out.Close()
	bufOut := bufio.NewWriter(out)

	//main loop
	log.Println("Merging results")
	for {
		minBuf, minIdx := -1, -1
		for i := range mergeBufs {

			if mergeIdx[i] == bufSize {
				//fill buf
				lines, _, _ := readChunk(readers[i], bufSize)
				mergeBufs[i] = lines
				mergeIdx[i] = 0
			}
			if mergeIdx[i] == len(mergeBufs[i]) {
				//file is empty
				continue
			}
			if minBuf == -1 && minIdx == -1 {
				minBuf = i
				minIdx = mergeIdx[i]
				continue
			}
			if strings.Compare(mergeBufs[minBuf][minIdx], mergeBufs[i][mergeIdx[i]]) > 0 {
				minBuf = i
				minIdx = mergeIdx[i]
			}
		}
		if minBuf == -1 && minIdx == -1 {
			break //no more lines
		}
		bufOut.WriteString(mergeBufs[minBuf][minIdx])
		mergeIdx[minBuf]++
	}
	bufOut.Flush()
	log.Println("Successfully sorted")
}

func generateTempFiles(infile string, chunkSize int) ([]*os.File, error) {
	log.Println("Start reading file")
	in, err := os.Open(infile)
	if err != nil {
		log.Printf("Failed to open %s. Reason: %s\n", infile, err.Error())
		return nil, err
	}
	defer in.Close()
	r := bufio.NewReader(in)

	tempFiles := make([]*os.File, 0)

	log.Println("Sorting chunks")

	more := true
	for more {
		lines, hasMore, err := readChunk(r, chunkSize)
		if err != nil {
			log.Printf("Error occured while reading %s. Error=%s\n", infile, err.Error())
			return tempFiles, err
		}
		more = hasMore
		sortChunk(lines)
		temp, err := writeToTemp(lines)
		if err != nil {
			log.Printf("Error creating temp file. Error=%s\n", err.Error())
			return tempFiles, err
		}
		tempFiles = append(tempFiles, temp)
		log.Printf("Generated temp file:%s\n", temp.Name())
	}
	return tempFiles, nil
}

func readChunk(r *bufio.Reader, chunkSize int) (lines []string, hasMore bool, err error) {
	hasMore = true
	lines = make([]string, chunkSize)
	for i := 0; i < chunkSize; i++ {
		line, err := r.ReadString('\n')
		if err != nil && err == io.EOF {
			lines = lines[:i]
			return lines, false, nil
		}
		if err != nil {
			return lines, false, err
		}
		lines[i] = string(line)
	}
	return
}

func sortChunk(data []string) {
	sort.Strings(data)
}

func writeToTemp(data []string) (*os.File, error) {
	f, err := ioutil.TempFile("", "sort")
	if err != nil {
		return nil, err
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	for _, line := range data {
		_, err := w.WriteString(line)
		if err != nil {
			return nil, err
		}
	}
	w.Flush()
	return f, nil
}

func deleteTempFiles(temp *[]*os.File) {
	log.Println("Deleting temp files.")
	for _, f := range *temp {
		err := os.Remove(f.Name())
		if err != nil {
			log.Printf("Error deleting file: %s\n", f.Name())
		}
	}
	log.Printf("Deleted %d files successfully.\n", len(*temp))
}
